# Skate Canada 2021 Live Online

Skate Canada is pleased to announce the start of a new production for Skate Canada hosted events which will be streamed live on CBC.ca and the CBC Gem App. We know our communities across Canada have greatly missed watching figure skating, which is wh